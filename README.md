1. Run MongoDb as a cluster to compatible with MongoWrite -> MongoItemWriterBuilder

npm install run-rs -g
run-rs -v 4.0.0 --shell

or Replace to MongoWrite -> ResourcelessTransactionManager

2. DB Configs:

url string: mongodb://localhost:27017/batch?replicaSet=rs
collection: product

3. CURL:

curl --location --request POST 'http://localhost:8080/start?fileLocation=sample-data.xlsx'

** Note: Spring web flux is not support to upload files via REST APIs.
https://github.com/spring-projects/spring-framework/issues/20738
https://github.com/spring-projects/spring-framework/issues/26386
https://hantsy.github.io/spring-reactive-sample/web/multipart.html


