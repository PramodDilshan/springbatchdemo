package dil.pdr.springbatchdemo.config;

import dil.pdr.springbatchdemo.model.Product;
import dil.pdr.springbatchdemo.step.MongoWriter;
import dil.pdr.springbatchdemo.step.ProductProcessor;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@AllArgsConstructor
public class JobConfiguration {

    @Autowired
    JobRepository jobRepository;

    @Autowired
    PlatformTransactionManager platformTransactionManager;

    @Autowired
    ProductProcessor productProcessor;

    @Autowired
    MongoWriter mongoWriter;

    public Job job(Step stepOne){
        return new JobBuilder("JobName" + System.currentTimeMillis(), jobRepository)
                .incrementer(new RunIdIncrementer())
                .start(stepOne)
                .build();
    }

    public Step stepOne(ItemReader<Product> excelReader){
        return new StepBuilder("stepOne", jobRepository)
                .<Product, Product>chunk(1, platformTransactionManager)
                .reader(excelReader)
                .processor(productProcessor)
                .writer(mongoWriter)
                .build();
    }

}
