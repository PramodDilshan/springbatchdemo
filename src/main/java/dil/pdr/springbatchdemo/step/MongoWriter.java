package dil.pdr.springbatchdemo.step;

import dil.pdr.springbatchdemo.model.Product;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.MongoItemWriter;
import org.springframework.batch.item.data.builder.MongoItemWriterBuilder;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MongoWriter implements ItemWriter<Product> {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void write(Chunk<? extends Product> chunk) throws Exception {
        compositeItemWriter().write(chunk);

    }

    private CompositeItemWriter<Product> compositeItemWriter(){
        CompositeItemWriter<Product> compositeItemWriter = new CompositeItemWriter<>();
        List<ItemWriter<? super Product>> writers = new ArrayList<>();
        writers.add(updatePriceWriter());
        compositeItemWriter.setDelegates(writers);
        return compositeItemWriter;
    }


    private MongoItemWriter<Product> updatePriceWriter() {
        return new MongoItemWriterBuilder<Product>()
                .collection("product")
                .template(mongoTemplate)
                .build();
    }
}
