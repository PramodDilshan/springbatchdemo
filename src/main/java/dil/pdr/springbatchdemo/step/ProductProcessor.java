package dil.pdr.springbatchdemo.step;

import dil.pdr.springbatchdemo.model.Product;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Component
public class ProductProcessor implements ItemProcessor<Product, Product> {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Product process(Product item) {
        Product product = mongoTemplate.query(Product.class)
                .matching(query(where("code").is(item.getCode())))
                .one()
                .orElse(null);

        if(product != null){
            product.setPrice(item.getPrice());
        }

        return product;
    }
}
