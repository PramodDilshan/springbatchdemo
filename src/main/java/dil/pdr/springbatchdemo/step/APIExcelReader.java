package dil.pdr.springbatchdemo.step;

import dil.pdr.springbatchdemo.model.Product;
import org.springframework.batch.extensions.excel.mapping.BeanWrapperRowMapper;
import org.springframework.batch.extensions.excel.poi.PoiItemReader;
import org.springframework.batch.extensions.excel.support.rowset.DefaultRowSetFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;


@Component
@Configuration
public class APIExcelReader implements ItemReader<Product> {

    private String file;

    private PoiItemReader<Product> delegate;

    @Override
    public Product read() throws Exception {
        if(delegate == null){
            delegate = excelReader();
            delegate.open(new ExecutionContext());
        }
        Product product = delegate.read();
        if(product == null){
            delegate.close();
            delegate = null;
        }
        return product;
    }

    public PoiItemReader<Product> excelReader() {
        PoiItemReader<Product> reader = new PoiItemReader<>();
        reader.setResource(new ClassPathResource(file));
        reader.setRowMapper(excelRowMapper());
        reader.setRowSetFactory(new DefaultRowSetFactory());
        reader.setLinesToSkip(1);
        return reader;
    }


    private BeanWrapperRowMapper<Product> excelRowMapper() {
        BeanWrapperRowMapper<Product> rowMapper = new BeanWrapperRowMapper<>();
        rowMapper.setTargetType(Product.class);
        return rowMapper;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
