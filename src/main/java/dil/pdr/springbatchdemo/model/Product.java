package dil.pdr.springbatchdemo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "product")
@Data
public class Product {

    @Id
    private String id;
    private String code;
    private Double price;
}
