package dil.pdr.springbatchdemo.controller;

import dil.pdr.springbatchdemo.step.APIExcelReader;
import dil.pdr.springbatchdemo.config.JobConfiguration;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;


@RestController
public class Controller {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    JobConfiguration jobConfiguration;

    @Autowired
    APIExcelReader apiExcelReader;

    @GetMapping(value = "/test")
    public Mono<ResponseEntity<String>> test(){
        return Mono.fromCallable(() -> ResponseEntity.status(HttpStatus.ACCEPTED).body("Hello"));
    }

    @PostMapping( value = "/start")
    public Mono<ResponseEntity<String>> startJob(@RequestParam("fileLocation") String file) {
        return Mono.fromCallable(() -> {
            apiExcelReader.setFile(file);
            Step step = jobConfiguration.stepOne(apiExcelReader);
            Job job = jobConfiguration.job(step);
            JobExecution jobExecution;

            try {
                jobExecution = jobLauncher.run(job,
                        new JobParametersBuilder().addString("fileName", file)
                        .toJobParameters()
                );
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
            return getResponse(jobExecution);
        });

    }

    private ResponseEntity<String> getResponse(JobExecution jobExecution){
        BatchStatus batchStatus = jobExecution.getStatus();
        String jobStatus = batchStatus.toString();
        if(batchStatus.isRunning()){
            return ResponseEntity.status(HttpStatus.CONTINUE).body(jobStatus);
        } else if (batchStatus.isUnsuccessful()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(jobStatus);
        }else {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(jobStatus);
        }

    }
}
